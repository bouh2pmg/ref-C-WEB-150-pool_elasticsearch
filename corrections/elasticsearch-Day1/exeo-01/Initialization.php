<?php
/**
 * Initialize data in PHP
 *
 * User: frederic
 * Date: 23/03/18
 * Time: 13:33
 */

include_once("Common.php");


$client = ElasticPdo::getClient();
if (!$client) {
    error_log("Cannot connect to Elasticsearch");
    exit(-1);
}
$count = 0;

// 1 to 31 resumes
// get resume from a file, line by line
$handleFile = @fopen("LoremIpsum.txt", 'r');
if (!$handleFile) {
    error_log("Cannot open file: LoremIpsum.txt");
    exit(-1);
}

// insert unitary resumes
for ($i = $count + 1; $i < 32; $i++) {
    $resume = new Resume("firstname" . $i, "lastname" . $i,
        $i . "-01-1970", fgets($handleFile), $i);
    $response = $client->index(ElasticPdo::getInsertParam($resume, $i));
    if (! ElasticPdo::getStatusActions($response)) {
        error_log("ERROR: INSERT ".$i." = " . json_encode($response));
    }
}

// Do wait a bit since insert is not immediate
sleep(1);

// get unitary resumes
for ($i = 1; $i < 32; $i++) {
    $response = $client->get(ElasticPdo::getSelectOrDeleteParam($i));
    if (ElasticPdo::getStatusActions($response)) {
        $resume = Resume::constructFromGetResponse($response);
        echo("OK: ".$i." = ".json_encode($resume->toArray())."\n");
    } else {
        error_log("ERROR: during get " . $i . " = " . json_encode($response));
    }
}
