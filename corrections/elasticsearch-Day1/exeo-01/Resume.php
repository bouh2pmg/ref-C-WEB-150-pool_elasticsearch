<?php
/**
 * Resume class containing different informations
 *
 * User: frederic
 * Date: 23/03/18
 * Time: 12:15
 */
include_once("Common.php");

/**
 * Class Resume
 */
class Resume
{
    var $firstname;
    var $lastname;
    var $birthdate;
    var $resume;
    var $experienceDuration;

    private static function createFromArray($source) {
        return new Resume($source['firstname'], $source['lastname'], $source['birthdate'],
            $source['resume'], $source['experienceDuration']);
    }

    /**
     * @param $response
     * @return Resume
     * @throws BadArgumentException if illegal values comes from Elasticsearch
     */
    public static function constructFromGetResponse(&$response) {
        return self::createFromArray(ElasticPdo::getElement($response));
    }

    /**
     * @param $response
     * @return Array of Resumes
     * @throws BadArgumentException if illegal values comes from Elasticsearch
     */
    public static function constructFromSearchResponses(&$response) {
        $array = array();
        if (ElasticPdo::getTotalHits($response) > 0) {
            $hits = ElasticPdo::getHits($response);
            foreach ($hits as $hit) {
                $array[] = self::createFromArray(ElasticPdo::getElement($hit));
            }
        }
        return $array;
    }

    /**
     * Resume constructor.
     * @param string $firstname
     * @param string $lastname
     * @param string $birthdate
     * @param string $resume
     * @param int $experienceDuration
     * @throws BadArgumentException if illegal argument
     */
    public function __construct(string $firstname, string $lastname, string $birthdate, string $resume,
                                int $experienceDuration)
    {
        $this->checkStringArguments("firstname", $firstname);
        $this->firstname = $firstname;
        $this->checkStringArguments("lastname", $lastname);
        $this->lastname = $lastname;
        $this->checkDateArguments("birthdate", $birthdate);
        $this->birthdate = $birthdate;
        $this->checkStringArguments("resume", $resume);
        $this->resume = $resume;
        $this->checkStringArguments("experienceDuration", $experienceDuration);
        $this->experienceDuration = $experienceDuration;
    }

    /**
     * @param string $name
     * @param string $arg
     * @throws BadArgumentException if illegal argument
     */
    private function checkStringArguments(string $name, string $arg)
    {
        if (is_string($arg) && strlen(trim($arg)) > 0) {
            return;
        }
        throw new BadArgumentException($name . " is not a valid string");
    }

    /**
     * @param string $name
     * @param string $arg
     * @throws BadArgumentException if illegal argument
     */
    private function checkDateArguments(string $name, string $arg)
    {
        $this->checkStringArguments($name, $arg);
        if (!strtotime($arg)) {
            throw new BadArgumentException($name . " is not a valid date");
        }
    }

    /**
     * @param string $name
     * @param int $arg
     * @throws BadArgumentException if illegal argument
     */
    private function checkIntArguments(string $name, int $arg)
    {
        if (is_int($arg) && $arg > 0) {
            return;
        }
        throw new BadArgumentException($name . " is not a valid int");
    }

    /**
     * @return array representation of this
     */
    public function toArray()
    {
        return get_object_vars($this);
    }
}