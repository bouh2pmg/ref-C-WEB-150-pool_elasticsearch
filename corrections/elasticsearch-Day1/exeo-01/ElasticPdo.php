<?php
/**
 * Elasticsearch PDO
 *
 * User: frederic
 * Date: 23/03/18
 * Time: 12:55
 */
include_once("Common.php");

use Elasticsearch\ClientBuilder;

/**
 * Class ElasticPdo
 *
 * Singleton to create one connexion to Elasticsearch as client.
 *
 * Note: the default here is the localhost only on default port
 */
class ElasticPdo
{
    static private $client = null;
    static private $defaultParams = [
        'index' => 'documents',
        'type' => 'resumes'
    ];

    /**
     * @return the Elasticsearch client or null in case of error
     */
    public static function getClient()
    {
        if (is_null(ElasticPdo::$client)) {
            ElasticPdo::$client = ClientBuilder::create()->build();
        }
        return ElasticPdo::$client;
    }


    /**
     * Helper to get array for Elasticsearch indexation
     *
     * @param Resume $resume
     * @param int $id
     * @return array
     */
    public static function getInsertParam(Resume &$resume, int $id)
    {
        $params = ElasticPdo::$defaultParams;
        $params['id'] = strval($id);
        $params['body'] = $resume->toArray();
        return $params;
    }

    /**
     * Helper to get array for Elasticsearch select
     *
     * @param int id
     * @return array
     */
    public static function getSelectOrDeleteParam(int $id)
    {
        $params = ElasticPdo::$defaultParams;
        $params['id'] = strval($id);
        return $params;
    }

    /**
     * Helper to get array for Elasticsearch find
     *
     * @param $query
     * @return array
     */
    public static function searchParam($query) {
        $params = ElasticPdo::$defaultParams;
        $params['body'] = [
                'query' => $query,
                'sort' => [ '_score'],
                'size' => 32
            ];
        return $params;
    }

    /**
     * Helper to get array for Elasticsearch update
     *
     * @param $query
     * @param int $id
     * @return array
     */
    public static function updateParam($query, $id) {
        $params = ElasticPdo::$defaultParams;
        $params['id'] = strval($id);
        $params['body'] = [
            'doc' => $query
        ];
        return $params;
    }

    /**
     * @param $response
     * @return True if ok, else False
     */
    public static function getStatusActions(&$response)
    {
        if (isset($response['result'])) {
            return strcasecmp($response['result'], 'created') == 0 ||
                strcasecmp($response['result'], 'updated') == 0 ||
                strcasecmp($response['result'], 'deleted') == 0;
        } elseif (isset($response['found'])) {
            return $response['found'];
        }
        return false;
    }

    /**
     * @param &$response
     * @return int number of responses (find)
     */
    public static function getTotalHits(&$response) {
        return $response['hits']['total'];
    }

    /**
     * @param &$response
     * @return array of real responses (without Id)
     */
    public static function getHits(&$response) {
        return $response['hits']['hits'];
    }

    /**
     * @param $hit
     * @return map containing values for the hit
     */
    public static function getElement(&$hit) {
        return $hit['_source'];
    }

    /**
     * @param $hit
     * @return id for the current hit
     */
    public static function getId(&$hit) {
        return $hit['_id'];
    }

    /**
     * @param $response
     * @return array of Ids
     */
    public static function getIds(&$response) {
        $hits = self::getHits($response);
        $result = array();
        foreach ($hits as $hit) {
            $result[] = self::getId($hit);
        }
        return $result;
    }
}