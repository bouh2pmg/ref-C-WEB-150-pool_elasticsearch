<?php
/**
 * Main class for project
 *
 * User: frederic
 * Date: 23/03/18
 * Time: 12:14
 */

include_once("Common.php");


$client = ElasticPdo::getClient();
if (!$client) {
    error_log("Cannot connect to Elasticsearch");
    exit(-1);
}


// search 'bras' among resumes and get Resume
$response = $client->search(ElasticPdo::searchParam(['match' => ['resume' => 'bras']]));
$resumes = Resume::constructFromSearchResponses($response);
if (count($resumes) != 2)  {
    error_log("ERROR: during find bras = " . json_encode($response));
}
foreach ($resumes as $resume) {
    echo("Find 'bras' = ".json_encode($resume->toArray())."\n");
}

// search 'bras' among resumes and delete corresponding Resume
$response = $client->search(ElasticPdo::searchParam(['match' => ['resume' => 'bras']]));
$ids = ElasticPdo::getIds($response);
if (count($ids) != 2)  {
    error_log("ERROR: during find bras before delete = " . json_encode($response));
}
foreach ($ids as $id) {
    $response = $client->delete(ElasticPdo::getSelectOrDeleteParam($id));
    if (! ElasticPdo::getStatusActions($response)) {
        error_log("ERROR: during delete item = " . $id." => ". json_encode($response));
    } else {
        echo "Resume ".$id." deleted\n";
    }
}


// Do wait a bit since delete is not immediate
sleep(1);


// search 'bras' among resumes but must not find them again
$response = $client->search(ElasticPdo::searchParam(['match' => ['resume' => 'bras']]));
if (ElasticPdo::getTotalHits($response) != 0) {
    error_log("ERROR: Should not find them => ". json_encode($response));
}

// search 'David Florence' among resumes and get Resume
$response = $client->search(ElasticPdo::searchParam(['match' => ['resume' => 'David Florence']]));
$resumes = Resume::constructFromSearchResponses($response);
if (count($resumes) != 18)  {
    error_log("ERROR: during find David Florence = " . json_encode($response));
}
foreach ($resumes as $resume) {
    echo("Find 'David Florence' = ".json_encode($resume->toArray())."\n");
}

// search 'David Florence' and at least 10 years of experience among resumes and get Resume
$response = $client->search(ElasticPdo::searchParam([ 'bool' => [ 'must' => [
    ['match' => ['resume' => 'David Florence']],
        ['range' => [ 'experienceDuration' => [ 'gte' => 10 ]]]]]]));
$resumes = Resume::constructFromSearchResponses($response);
if (count($resumes) != 13)  {
    error_log("ERROR: during find David Florence and gte 10 = " . json_encode($response));
}
foreach ($resumes as $resume) {
    echo("Find 'David Florence' and gte 10 = ".json_encode($resume->toArray())."\n");
}


// now update the first '1' item and check values before and after
$response = $client->get(ElasticPdo::getSelectOrDeleteParam(1));
if (ElasticPdo::getStatusActions($response)) {
    $resume = Resume::constructFromGetResponse($response);
    if (strcmp($resume->lastname, 'lastname1') != 0) {
        error_log("ERROR: Should be lowercase => ". json_encode($response));
    }
}
$response = $client->update(ElasticPdo::updateParam(['lastname' => 'Lastname1', 'firstname' => 'Firstname1'], 1));
if (ElasticPdo::getStatusActions($response)) {
    // Do wait a bit since update is not immediate
    sleep(1);

    $response = $client->get(ElasticPdo::getSelectOrDeleteParam(1));
    $resume = Resume::constructFromGetResponse($response);
    if (strcmp($resume->lastname, 'Lastname1') != 0) {
        error_log("ERROR: Should be Uppercase => ". json_encode($response));
    }
}

