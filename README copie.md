# This contains the 3 days on Elasticsearch using PHP CLI, Laravel and Javascript

- 1rst half of 1rst day is an introduction to Elasticsearch using curl and Head
- 2nd hald of 1rst day is Using PHP CLI to use Elasticsearch
- Second and Third days are in a project mode, first using Laravel, introducing API REST, then Javascript in the HTML part of the very same Laravel project

# Corrections

They are only available for PHP CLI in directory corrections/elasticsearch-Day1 .

Note that the directory corrections/elasticsearch-Day2-3 contains the corrected project as example on how to use Elasticsearch within Laravel.

Note also that some other ways exist, such as using Scout extensions, but for an unknown reason, there seems to not be as up-to-date as necessary, since they do not reflect the major changes from Elasticsearch 2 to Elasticsearch 5 then 6:

- Index has a unique mapping and it is not possible to change it once created, except dynamically by adding new fields
- String type is replaced by Text or Keyword types

Therefore one could update in an easier way once one of those existing packages is really up-to-date since they are intend to really simplify the process to associate a PDO to Elasticsearch.

